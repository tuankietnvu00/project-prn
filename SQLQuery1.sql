create database BookStoreManagement


CREATE TABLE BookCategory (
    BookCategoryId int PRIMARY KEY,
    BookGenreType varchar(255),
    Description text
);

select * from BookCategory

INSERT INTO BookCategory (BookCategoryId, BookGenreType, Description) VALUES
(1, 'Fiction', 'Books that are invented, made up, or imaginary.'),
(2, 'Non-Fiction', 'Books that are based on facts, real events, and real people.'),
(3, 'Science Fiction', 'Books that often involve futuristic science and technology.'),
(4, 'Mystery', 'Books that involve a crime or puzzle to be solved.'),
(5, 'Romance', 'Books that focus on romantic love relationships.');

CREATE TABLE Book (
    BookId int PRIMARY KEY,
    BookName varchar(255),
    Description text,
    PublicationDate datetime,
    Quantity int,
    Price money,
    Author varchar(255),
    BookCategoryId int,
    UnitInStock int,
    UnitPrice money,
    FOREIGN KEY (BookCategoryId) REFERENCES BookCategory(BookCategoryId)
);
ALTER TABLE Book

DROP COLUMN UnitInStock;
DROP COLUMN UnitPrice

INSERT INTO Book (BookId, BookName, Description, PublicationDate, Quantity, Price, Author, BookCategoryId, UnitInStock, UnitPrice) VALUES
(1, 'To Kill a Mockingbird', 'A novel by Harper Lee', '1960-07-11', 100, 12.99, 'Harper Lee', 1, 100, 12.99),
(2, '1984', 'A dystopian novel by George Orwell', '1949-06-08', 150, 10.99, 'George Orwell', 1, 150, 10.99),
(3, 'Sapiens: A Brief History of Humankind', 'A book by Yuval Noah Harari', '2011-05-15', 80, 15.99, 'Yuval Noah Harari', 2, 80, 15.99),
(4, 'The Martian', 'A science fiction novel by Andy Weir', '2014-02-11', 120, 14.99, 'Andy Weir', 3, 120, 14.99),
(5, 'The Da Vinci Code', 'A mystery-detective novel by Dan Brown', '2003-03-18', 90, 11.99, 'Dan Brown', 4, 90, 11.99),
(6, 'Pride and Prejudice', 'A romantic novel by Jane Austen', '1813-01-28', 110, 9.99, 'Jane Austen', 5, 110, 9.99);
select * from Book

ALTER TABLE Customer
ADD Balance money;

ALTER TABLE Customer
ADD MemberRole bit;


CREATE TABLE Customer (
    CustomerId int PRIMARY KEY,
    Email varchar(255),
    Password varchar(255),
    Name varchar(255),
	Balance money,
	MemberRole bit
);

INSERT INTO Customer (CustomerId, Email, Password, Name, Balance, MemberRole) VALUES
(1, 'kiet@gmail.com', '123', 'Tuan Kiet', 100.00, 1), 
(2, 'trinh@gmail.com', '123', 'Lam Trinh', 150.00, 0),
(3, 'bach@gmail.com', '123', 'Xuan Bach', 200.00, 0),
(4, 'manh@gmail.com', '123', 'Duc Manh', 200.00, 0),
(5, 'linh@gmail.com', '123', 'Khanh Linh', 80.00, 0); 


select * from Customer

CREATE TABLE [Order] (
    OrderId int PRIMARY KEY,
    CustomerId int,
    OrderDate datetime,
    Price money,
    FOREIGN KEY (CustomerId) REFERENCES Customer(CustomerId)
);

select * from [Order]

CREATE TABLE OrderDetail (
    OrderId int,
    BookId int,
    UnitPrice money,
    Quantity int,
    PRIMARY KEY (OrderId, BookId),
    FOREIGN KEY (OrderId) REFERENCES [Order](OrderId),
    FOREIGN KEY (BookId) REFERENCES Book(BookId)
);