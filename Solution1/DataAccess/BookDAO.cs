﻿using DataAccess.DataAccess;
using DataAccess.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class BookDAO
    {
        private BookRepository _repo = new BookRepository();
        public List<Book> GetAllBooks()
        {
            return _repo.GetAllBooks();
        }

        public List<Book> SearchBooksById(int id) { 
            return _repo.GetAllBooks().Where(x => x.BookId == id).ToList();
        }
        //public List<Book> SearchBooksByName(String keyword) { }

        //public List<Book> SearchBooksByDescription(String keyword) { }
        //public List<Book> SearchBooksByAuthor(String keyword) { }

        public void DeleteABook(int id)
        {
            _repo.DeleteBook(id);

        }
    }
}
