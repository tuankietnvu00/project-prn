﻿using DataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class CustomerRepository
    {
       public Customer? getCustomerByEmail(string email)
        {
            BookStoreManagementContext context = new BookStoreManagementContext();
            return context.Customers.FirstOrDefault(x => x.Email == email);
        }
    }
}
