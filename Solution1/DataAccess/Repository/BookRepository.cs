﻿using DataAccess.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class BookRepository
    {
        private BookStoreManagementContext _context;
        public Book? GetBookById(int bookId)
        {
            _context = new BookStoreManagementContext();
            //return _context.Books.FirstOrDefault(x => x.BookId == bookId);
            return _context.Books.Find(bookId);
        }

        public List<Book> GetAllBooks()
        {
            _context = new BookStoreManagementContext();
            //return _context.Books.ToList();
            return _context.Books.Include(cat => cat.BookCategory).ToList();
        }

        public void CreateBook(Book book)
        {
            _context = new BookStoreManagementContext();
            _context.Books.Add(book);
            _context.SaveChanges();

        }

        public void UpdateBook(Book book)
        {
            _context = new BookStoreManagementContext();
            _context.Books.Update(book);
            _context.SaveChanges();

        }

        public void DeleteBook(int bookId)
        {
            _context = new BookStoreManagementContext();

            var book = _context.Books.FirstOrDefault(x => x.BookId == bookId);

            if(book != null) {
                
                _context.Books.Remove(book);
                _context.SaveChanges();
            }

        }

        


    }
}
