﻿using DataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class BookCategoryRepository
    {
        private BookStoreManagementContext _context;
        public List<BookCategory> GetAllCategories()
        {
            _context = new BookStoreManagementContext();
            return _context.BookCategories.ToList();

        }
    }
}
