﻿using DataAccess.DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class CustomerDAO
    {
        public Customer CheckLogin(string email, string password)
        {
            CustomerRepository repository = new CustomerRepository();

            Customer account = repository.getCustomerByEmail(email);

            if (account == null)
            {
                return null; //email khong ton tai
            }
            if (account.Password == password)
            {
                return account;
            }
            return null;
        }   
    }
}
