﻿using System;
using System.Collections.Generic;

namespace DataAccess.DataAccess
{
    public partial class Customer
    {
        public Customer()
        {
            Orders = new HashSet<Order>();
        }

        public int CustomerId { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Name { get; set; }
        public decimal? Balance { get; set; }
        public bool MemberRole { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
