﻿using System;
using System.Collections.Generic;

namespace DataAccess.DataAccess
{
    public partial class BookCategory
    {
        public BookCategory()
        {
            Books = new HashSet<Book>();
        }

        public int BookCategoryId { get; set; }
        public string? BookGenreType { get; set; }
        public string? Description { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
