﻿using System;
using System.Collections.Generic;

namespace DataAccess.DataAccess
{
    public partial class Book
    {
        public Book()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int BookId { get; set; }
        public string? BookName { get; set; }
        public string? Description { get; set; }
        public DateTime? PublicationDate { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public string? Author { get; set; }
        public int? BookCategoryId { get; set; }


        public virtual BookCategory? BookCategory { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
