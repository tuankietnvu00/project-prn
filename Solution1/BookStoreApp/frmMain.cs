using DataAccess;
using DataAccess.DataAccess;

namespace BookStoreApp
{
    public partial class frmMain : Form
    {

        private BookDAO _bookDAO = new BookDAO();
        private BookCategoryDAO _categoryDAO = new BookCategoryDAO();

        public frmMain()
        {
            InitializeComponent();
        }



        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            var result = _bookDAO.GetAllBooks();
            dgvBookList.DataSource = null;
            dgvBookList.DataSource = result;

            dgvBookList.Columns["OrderDetails"].Visible = false;

            //do category vao combo box
            cboCategory.DataSource = _categoryDAO.GetAllCategories();
            cboCategory.DisplayMember = "BookGenreType";
            cboCategory.ValueMember = "BookCategoryId";



        }

        private void dgvBookList_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvBookList.SelectedRows.Count > 0)
            {
                var selectedBook = (Book)dgvBookList.SelectedRows[0].DataBoundItem;
                cboCategory.SelectedValue = selectedBook.BookCategoryId;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSearch.Text))
            {
                MessageBox.Show("The search keyword is required!!!!", "Keyword required", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var result = _bookDAO.SearchBooksById(int.Parse(txtSearch.Text));
            if (result.Count == 0)
            {
                MessageBox.Show("The book not exist!!!!", "Not found", MessageBoxButtons.OK);
            }

            dgvBookList.DataSource = null;
            dgvBookList.DataSource = result;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var result = _bookDAO.GetAllBooks();
            dgvBookList.DataSource = null;
            dgvBookList.DataSource = result;

            dgvBookList.Columns["OrderDetails"].Visible = false;
        }

        private void btnOrderList_Click(object sender, EventArgs e)
        {
            frmOrders order = new frmOrders();
            order.Show();
            this.Hide();
        }

        private void btnCustomerList_Click(object sender, EventArgs e)
        {
            frmCustomers customer = new frmCustomers();
            customer.Show();
            this.Hide();
        }

        private void btnUpdateBook_Click(object sender, EventArgs e)
        {

        }

        private void btnDeleteBook_Click(object sender, EventArgs e)
        {

        }

        private void btnCreateBook_Click(object sender, EventArgs e)
        {
            frmBookInfo bookInfo = new frmBookInfo()
            {

            };
            bookInfo.Show();

        }

        private Book GetBook(int id)
        {

        }
    }
}
