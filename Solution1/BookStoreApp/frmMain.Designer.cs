﻿namespace BookStoreApp
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblHeader = new Label();
            grbSearch = new GroupBox();
            btnLoad = new Button();
            cboCategory = new ComboBox();
            cboSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            dgvBookList = new DataGridView();
            label1 = new Label();
            btnCreateBook = new Button();
            btnUpdateBook = new Button();
            btnDeleteBook = new Button();
            btnExit = new Button();
            btnOrderList = new Button();
            btnCustomerList = new Button();
            grbSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvBookList).BeginInit();
            SuspendLayout();
            // 
            // lblHeader
            // 
            lblHeader.AutoSize = true;
            lblHeader.Font = new Font("Segoe UI", 36F, FontStyle.Bold, GraphicsUnit.Point);
            lblHeader.ForeColor = Color.Crimson;
            lblHeader.Location = new Point(-9, -3);
            lblHeader.Name = "lblHeader";
            lblHeader.Size = new Size(347, 81);
            lblHeader.TabIndex = 0;
            lblHeader.Text = "Book Store";
            // 
            // grbSearch
            // 
            grbSearch.Controls.Add(btnLoad);
            grbSearch.Controls.Add(cboCategory);
            grbSearch.Controls.Add(cboSearch);
            grbSearch.Controls.Add(txtSearch);
            grbSearch.Controls.Add(btnSearch);
            grbSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            grbSearch.ForeColor = Color.Crimson;
            grbSearch.Location = new Point(39, 81);
            grbSearch.Name = "grbSearch";
            grbSearch.Size = new Size(829, 148);
            grbSearch.TabIndex = 1;
            grbSearch.TabStop = false;
            grbSearch.Text = "Search Criteria";
            // 
            // btnLoad
            // 
            btnLoad.FlatStyle = FlatStyle.Flat;
            btnLoad.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnLoad.ForeColor = Color.Crimson;
            btnLoad.Location = new Point(25, 99);
            btnLoad.Name = "btnLoad";
            btnLoad.Size = new Size(139, 34);
            btnLoad.TabIndex = 12;
            btnLoad.Text = "Load";
            btnLoad.UseVisualStyleBackColor = true;
            btnLoad.Click += btnLoad_Click;
            // 
            // cboCategory
            // 
            cboCategory.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cboCategory.FormattingEnabled = true;
            cboCategory.Location = new Point(177, 97);
            cboCategory.Name = "cboCategory";
            cboCategory.Size = new Size(178, 36);
            cboCategory.TabIndex = 11;
            // 
            // cboSearch
            // 
            cboSearch.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cboSearch.FormattingEnabled = true;
            cboSearch.Location = new Point(664, 45);
            cboSearch.Name = "cboSearch";
            cboSearch.Size = new Size(122, 36);
            cboSearch.TabIndex = 10;
            // 
            // txtSearch
            // 
            txtSearch.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtSearch.Location = new Point(177, 45);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(481, 34);
            txtSearch.TabIndex = 9;
            // 
            // btnSearch
            // 
            btnSearch.FlatStyle = FlatStyle.Flat;
            btnSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnSearch.ForeColor = Color.Crimson;
            btnSearch.Location = new Point(25, 45);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(139, 34);
            btnSearch.TabIndex = 8;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // dgvBookList
            // 
            dgvBookList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvBookList.Location = new Point(39, 313);
            dgvBookList.Name = "dgvBookList";
            dgvBookList.RowHeadersWidth = 51;
            dgvBookList.RowTemplate.Height = 29;
            dgvBookList.Size = new Size(829, 306);
            dgvBookList.TabIndex = 2;
            dgvBookList.SelectionChanged += dgvBookList_SelectionChanged;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.ForeColor = Color.Crimson;
            label1.Location = new Point(39, 269);
            label1.Name = "label1";
            label1.Size = new Size(147, 41);
            label1.TabIndex = 3;
            label1.Text = "Book List";
            // 
            // btnCreateBook
            // 
            btnCreateBook.FlatStyle = FlatStyle.Flat;
            btnCreateBook.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnCreateBook.ForeColor = Color.Crimson;
            btnCreateBook.Location = new Point(928, 313);
            btnCreateBook.Name = "btnCreateBook";
            btnCreateBook.Size = new Size(161, 40);
            btnCreateBook.TabIndex = 9;
            btnCreateBook.Text = "Create";
            btnCreateBook.UseVisualStyleBackColor = true;
            btnCreateBook.Click += btnCreateBook_Click;
            // 
            // btnUpdateBook
            // 
            btnUpdateBook.FlatStyle = FlatStyle.Flat;
            btnUpdateBook.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnUpdateBook.ForeColor = Color.Crimson;
            btnUpdateBook.Location = new Point(928, 392);
            btnUpdateBook.Name = "btnUpdateBook";
            btnUpdateBook.Size = new Size(161, 40);
            btnUpdateBook.TabIndex = 10;
            btnUpdateBook.Text = "Update";
            btnUpdateBook.UseVisualStyleBackColor = true;
            btnUpdateBook.Click += btnUpdateBook_Click;
            // 
            // btnDeleteBook
            // 
            btnDeleteBook.FlatStyle = FlatStyle.Flat;
            btnDeleteBook.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnDeleteBook.ForeColor = Color.Crimson;
            btnDeleteBook.Location = new Point(928, 472);
            btnDeleteBook.Name = "btnDeleteBook";
            btnDeleteBook.Size = new Size(161, 40);
            btnDeleteBook.TabIndex = 11;
            btnDeleteBook.Text = "Delete";
            btnDeleteBook.UseVisualStyleBackColor = true;
            btnDeleteBook.Click += btnDeleteBook_Click;
            // 
            // btnExit
            // 
            btnExit.FlatStyle = FlatStyle.Flat;
            btnExit.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnExit.ForeColor = Color.Crimson;
            btnExit.Location = new Point(928, 579);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(161, 40);
            btnExit.TabIndex = 12;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // btnOrderList
            // 
            btnOrderList.FlatStyle = FlatStyle.Flat;
            btnOrderList.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnOrderList.ForeColor = Color.Crimson;
            btnOrderList.Location = new Point(928, 93);
            btnOrderList.Name = "btnOrderList";
            btnOrderList.Size = new Size(161, 40);
            btnOrderList.TabIndex = 13;
            btnOrderList.Text = "Order List";
            btnOrderList.UseVisualStyleBackColor = true;
            btnOrderList.Click += btnOrderList_Click;
            // 
            // btnCustomerList
            // 
            btnCustomerList.FlatStyle = FlatStyle.Flat;
            btnCustomerList.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnCustomerList.ForeColor = Color.Crimson;
            btnCustomerList.Location = new Point(928, 189);
            btnCustomerList.Name = "btnCustomerList";
            btnCustomerList.Size = new Size(161, 40);
            btnCustomerList.TabIndex = 14;
            btnCustomerList.Text = "Customer List";
            btnCustomerList.UseVisualStyleBackColor = true;
            btnCustomerList.Click += btnCustomerList_Click;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(1129, 655);
            Controls.Add(btnCustomerList);
            Controls.Add(btnOrderList);
            Controls.Add(btnExit);
            Controls.Add(btnDeleteBook);
            Controls.Add(btnUpdateBook);
            Controls.Add(btnCreateBook);
            Controls.Add(label1);
            Controls.Add(dgvBookList);
            Controls.Add(grbSearch);
            Controls.Add(lblHeader);
            Name = "frmMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Main";
            FormClosed += frmMain_FormClosed;
            Load += frmMain_Load;
            grbSearch.ResumeLayout(false);
            grbSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dgvBookList).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblHeader;
        private GroupBox grbSearch;
        private DataGridView dgvBookList;
        private Label label1;
        private Button btnSearch;
        private ComboBox cboSearch;
        private TextBox txtSearch;
        private Button btnCreateBook;
        private Button btnUpdateBook;
        private Button btnDeleteBook;
        private Button btnExit;
        private Button btnOrderList;
        private Button btnCustomerList;
        private ComboBox cboCategory;
        private Button btnLoad;
    }
}
