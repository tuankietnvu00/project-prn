﻿namespace BookStoreApp
{
    partial class frmUserFunction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnExit = new Button();
            btnBuy = new Button();
            label1 = new Label();
            dgvBookList = new DataGridView();
            grbSearch = new GroupBox();
            cboSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            lblHeader = new Label();
            txtBalance = new TextBox();
            label2 = new Label();
            ((System.ComponentModel.ISupportInitialize)dgvBookList).BeginInit();
            grbSearch.SuspendLayout();
            SuspendLayout();
            // 
            // btnExit
            // 
            btnExit.FlatStyle = FlatStyle.Flat;
            btnExit.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnExit.ForeColor = Color.Crimson;
            btnExit.Location = new Point(960, 597);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(161, 40);
            btnExit.TabIndex = 22;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            // 
            // btnBuy
            // 
            btnBuy.FlatStyle = FlatStyle.Flat;
            btnBuy.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnBuy.ForeColor = Color.Crimson;
            btnBuy.Location = new Point(960, 331);
            btnBuy.Name = "btnBuy";
            btnBuy.Size = new Size(161, 40);
            btnBuy.TabIndex = 19;
            btnBuy.Text = "Buy";
            btnBuy.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.ForeColor = Color.Crimson;
            label1.Location = new Point(71, 287);
            label1.Name = "label1";
            label1.Size = new Size(147, 41);
            label1.TabIndex = 18;
            label1.Text = "Book List";
            // 
            // dgvBookList
            // 
            dgvBookList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvBookList.Location = new Point(71, 331);
            dgvBookList.Name = "dgvBookList";
            dgvBookList.RowHeadersWidth = 51;
            dgvBookList.RowTemplate.Height = 29;
            dgvBookList.Size = new Size(829, 306);
            dgvBookList.TabIndex = 17;
            // 
            // grbSearch
            // 
            grbSearch.Controls.Add(cboSearch);
            grbSearch.Controls.Add(txtSearch);
            grbSearch.Controls.Add(btnSearch);
            grbSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            grbSearch.ForeColor = Color.Crimson;
            grbSearch.Location = new Point(71, 99);
            grbSearch.Name = "grbSearch";
            grbSearch.Size = new Size(829, 148);
            grbSearch.TabIndex = 16;
            grbSearch.TabStop = false;
            grbSearch.Text = "Search Criteria";
            // 
            // cboSearch
            // 
            cboSearch.FormattingEnabled = true;
            cboSearch.Location = new Point(177, 66);
            cboSearch.Name = "cboSearch";
            cboSearch.Size = new Size(122, 36);
            cboSearch.TabIndex = 10;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(305, 68);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(481, 34);
            txtSearch.TabIndex = 9;
            // 
            // btnSearch
            // 
            btnSearch.FlatStyle = FlatStyle.Flat;
            btnSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnSearch.ForeColor = Color.Crimson;
            btnSearch.Location = new Point(29, 66);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(139, 34);
            btnSearch.TabIndex = 8;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            // 
            // lblHeader
            // 
            lblHeader.AutoSize = true;
            lblHeader.Font = new Font("Segoe UI", 36F, FontStyle.Bold, GraphicsUnit.Point);
            lblHeader.ForeColor = Color.Crimson;
            lblHeader.Location = new Point(23, 15);
            lblHeader.Name = "lblHeader";
            lblHeader.Size = new Size(347, 81);
            lblHeader.TabIndex = 15;
            lblHeader.Text = "Book Store";
            // 
            // txtBalance
            // 
            txtBalance.BackColor = SystemColors.ActiveCaption;
            txtBalance.BorderStyle = BorderStyle.None;
            txtBalance.Location = new Point(960, 35);
            txtBalance.Name = "txtBalance";
            txtBalance.ReadOnly = true;
            txtBalance.Size = new Size(161, 20);
            txtBalance.TabIndex = 25;
            txtBalance.Text = "abc";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label2.ForeColor = Color.Crimson;
            label2.Location = new Point(854, 27);
            label2.Name = "label2";
            label2.Size = new Size(86, 28);
            label2.TabIndex = 26;
            label2.Text = "Balance";
            // 
            // frmUserFunction
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(1175, 667);
            Controls.Add(label2);
            Controls.Add(txtBalance);
            Controls.Add(btnExit);
            Controls.Add(btnBuy);
            Controls.Add(label1);
            Controls.Add(dgvBookList);
            Controls.Add(grbSearch);
            Controls.Add(lblHeader);
            Name = "frmUserFunction";
            Text = "BookStore";
            ((System.ComponentModel.ISupportInitialize)dgvBookList).EndInit();
            grbSearch.ResumeLayout(false);
            grbSearch.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnCustomerList;
        private Button btnOrderList;
        private Button btnExit;
        private Button btnDeleteBook;
        private Button btnUpdateBook;
        private Button btnBuy;
        private Label label1;
        private DataGridView dgvBookList;
        private GroupBox grbSearch;
        private ComboBox cboSearch;
        private TextBox txtSearch;
        private Button btnSearch;
        private Label lblHeader;
        private TextBox txtBalance;
        private Label label2;
    }
}