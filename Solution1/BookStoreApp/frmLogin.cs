﻿using DataAccess;
using DataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStoreApp
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string email = txtEmailLogin.Text;
            string password = txtPasswordLogin.Text;

            CustomerDAO customerDAO = new CustomerDAO();
            Customer account = customerDAO.CheckLogin(email, password);

            if (account == null)
            {
                MessageBox.Show("Login failed. Check your username or password","Wrong information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(account.MemberRole == true) {
                frmMain main= new frmMain();
                main.Show();
                this.Hide();
            }
            if (account.MemberRole == false)
            {
                frmUserFunction userFunction = new frmUserFunction();
                userFunction.Show();
                this.Hide();
            }

        }
    }
}
