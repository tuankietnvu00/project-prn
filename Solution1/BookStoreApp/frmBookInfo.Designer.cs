﻿namespace BookStoreApp
{
    partial class frmBookInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            grbBookInfo = new GroupBox();
            cboCategory = new ComboBox();
            dtpReleasedDate = new DateTimePicker();
            rtxtBookDescription = new RichTextBox();
            txtAuthor = new TextBox();
            txtPrice = new TextBox();
            txtQuantity = new TextBox();
            txtBookName = new TextBox();
            txtIDBook = new TextBox();
            label8 = new Label();
            label7 = new Label();
            label6 = new Label();
            label5 = new Label();
            label4 = new Label();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            btnOK = new Button();
            btnCancle = new Button();
            grbBookInfo.SuspendLayout();
            SuspendLayout();
            // 
            // grbBookInfo
            // 
            grbBookInfo.Controls.Add(cboCategory);
            grbBookInfo.Controls.Add(dtpReleasedDate);
            grbBookInfo.Controls.Add(rtxtBookDescription);
            grbBookInfo.Controls.Add(txtAuthor);
            grbBookInfo.Controls.Add(txtPrice);
            grbBookInfo.Controls.Add(txtQuantity);
            grbBookInfo.Controls.Add(txtBookName);
            grbBookInfo.Controls.Add(txtIDBook);
            grbBookInfo.Controls.Add(label8);
            grbBookInfo.Controls.Add(label7);
            grbBookInfo.Controls.Add(label6);
            grbBookInfo.Controls.Add(label5);
            grbBookInfo.Controls.Add(label4);
            grbBookInfo.Controls.Add(label3);
            grbBookInfo.Controls.Add(label2);
            grbBookInfo.Controls.Add(label1);
            grbBookInfo.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            grbBookInfo.ForeColor = Color.Crimson;
            grbBookInfo.Location = new Point(23, 12);
            grbBookInfo.Name = "grbBookInfo";
            grbBookInfo.Size = new Size(515, 518);
            grbBookInfo.TabIndex = 0;
            grbBookInfo.TabStop = false;
            grbBookInfo.Text = "Book Info";
            // 
            // cboCategory
            // 
            cboCategory.FormattingEnabled = true;
            cboCategory.Location = new Point(164, 443);
            cboCategory.Name = "cboCategory";
            cboCategory.Size = new Size(301, 36);
            cboCategory.TabIndex = 36;
            // 
            // dtpReleasedDate
            // 
            dtpReleasedDate.CalendarFont = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dtpReleasedDate.CustomFormat = "";
            dtpReleasedDate.Font = new Font("Segoe UI", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            dtpReleasedDate.Location = new Point(164, 279);
            dtpReleasedDate.Name = "dtpReleasedDate";
            dtpReleasedDate.Size = new Size(301, 30);
            dtpReleasedDate.TabIndex = 35;
            // 
            // rtxtBookDescription
            // 
            rtxtBookDescription.Location = new Point(164, 144);
            rtxtBookDescription.Name = "rtxtBookDescription";
            rtxtBookDescription.Size = new Size(301, 120);
            rtxtBookDescription.TabIndex = 34;
            rtxtBookDescription.Text = "";
            // 
            // txtAuthor
            // 
            txtAuthor.Location = new Point(164, 395);
            txtAuthor.Name = "txtAuthor";
            txtAuthor.Size = new Size(301, 34);
            txtAuthor.TabIndex = 32;
            // 
            // txtPrice
            // 
            txtPrice.Location = new Point(347, 340);
            txtPrice.Name = "txtPrice";
            txtPrice.Size = new Size(118, 34);
            txtPrice.TabIndex = 31;
            // 
            // txtQuantity
            // 
            txtQuantity.Location = new Point(164, 340);
            txtQuantity.Name = "txtQuantity";
            txtQuantity.Size = new Size(120, 34);
            txtQuantity.TabIndex = 30;
            // 
            // txtBookName
            // 
            txtBookName.Location = new Point(164, 85);
            txtBookName.Name = "txtBookName";
            txtBookName.Size = new Size(301, 34);
            txtBookName.TabIndex = 27;
            // 
            // txtIDBook
            // 
            txtIDBook.Location = new Point(164, 39);
            txtIDBook.Name = "txtIDBook";
            txtIDBook.Size = new Size(301, 34);
            txtIDBook.TabIndex = 26;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label8.ForeColor = Color.Crimson;
            label8.Location = new Point(25, 458);
            label8.Name = "label8";
            label8.Size = new Size(83, 21);
            label8.TabIndex = 25;
            label8.Text = "Category";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label7.ForeColor = Color.Crimson;
            label7.Location = new Point(290, 346);
            label7.Name = "label7";
            label7.Size = new Size(51, 21);
            label7.TabIndex = 24;
            label7.Text = "Price";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label6.ForeColor = Color.Crimson;
            label6.Location = new Point(25, 401);
            label6.Name = "label6";
            label6.Size = new Size(62, 21);
            label6.TabIndex = 23;
            label6.Text = "Author";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label5.ForeColor = Color.Crimson;
            label5.Location = new Point(25, 346);
            label5.Name = "label5";
            label5.Size = new Size(77, 21);
            label5.TabIndex = 22;
            label5.Text = "Quantity";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label4.ForeColor = Color.Crimson;
            label4.Location = new Point(25, 285);
            label4.Name = "label4";
            label4.Size = new Size(129, 21);
            label4.TabIndex = 21;
            label4.Text = "Released Date";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label3.ForeColor = Color.Crimson;
            label3.Location = new Point(25, 144);
            label3.Name = "label3";
            label3.Size = new Size(100, 21);
            label3.TabIndex = 20;
            label3.Text = "Description";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label2.ForeColor = Color.Crimson;
            label2.Location = new Point(25, 91);
            label2.Name = "label2";
            label2.Size = new Size(57, 21);
            label2.TabIndex = 19;
            label2.Text = "Name";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Arial", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label1.ForeColor = Color.Crimson;
            label1.Location = new Point(25, 45);
            label1.Name = "label1";
            label1.Size = new Size(27, 21);
            label1.TabIndex = 18;
            label1.Text = "ID";
            // 
            // btnOK
            // 
            btnOK.FlatStyle = FlatStyle.Flat;
            btnOK.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnOK.ForeColor = Color.Crimson;
            btnOK.Location = new Point(119, 551);
            btnOK.Name = "btnOK";
            btnOK.Size = new Size(94, 29);
            btnOK.TabIndex = 1;
            btnOK.Text = "OK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += btnOK_Click;
            // 
            // btnCancle
            // 
            btnCancle.FlatStyle = FlatStyle.Flat;
            btnCancle.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancle.ForeColor = Color.Crimson;
            btnCancle.Location = new Point(336, 551);
            btnCancle.Name = "btnCancle";
            btnCancle.Size = new Size(94, 29);
            btnCancle.TabIndex = 2;
            btnCancle.Text = "Cancle";
            btnCancle.UseVisualStyleBackColor = true;
            // 
            // frmBookInfo
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(568, 609);
            Controls.Add(btnCancle);
            Controls.Add(btnOK);
            Controls.Add(grbBookInfo);
            Name = "frmBookInfo";
            Text = "frmBookDetail";
            Load += frmBookInfo_Load;
            grbBookInfo.ResumeLayout(false);
            grbBookInfo.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox grbBookInfo;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private DateTimePicker dtpReleasedDate;
        private RichTextBox rtxtBookDescription;
        private TextBox textBox8;
        private TextBox txtAuthor;
        private TextBox txtPrice;
        private TextBox txtQuantity;
        private TextBox txtBookName;
        private TextBox txtIDBook;
        private Button btnOK;
        private ComboBox cboCategory;
        private Button btnCancle;
    }
}