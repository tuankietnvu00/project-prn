﻿namespace BookStoreApp
{
    partial class frmOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblHeader = new Label();
            btnCustomerList = new Button();
            btnMain = new Button();
            btnDeleteOrder = new Button();
            btnUpdateOrder = new Button();
            btnCreateOrder = new Button();
            label1 = new Label();
            dgvOrderList = new DataGridView();
            grbSearch = new GroupBox();
            cboSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            btnExit = new Button();
            btnOrderDetail = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvOrderList).BeginInit();
            grbSearch.SuspendLayout();
            SuspendLayout();
            // 
            // lblHeader
            // 
            lblHeader.AutoSize = true;
            lblHeader.Font = new Font("Segoe UI", 36F, FontStyle.Bold, GraphicsUnit.Point);
            lblHeader.ForeColor = Color.Crimson;
            lblHeader.Location = new Point(12, 9);
            lblHeader.Name = "lblHeader";
            lblHeader.Size = new Size(347, 81);
            lblHeader.TabIndex = 1;
            lblHeader.Text = "Book Store";
            // 
            // btnCustomerList
            // 
            btnCustomerList.FlatStyle = FlatStyle.Flat;
            btnCustomerList.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnCustomerList.ForeColor = Color.Crimson;
            btnCustomerList.Location = new Point(921, 201);
            btnCustomerList.Name = "btnCustomerList";
            btnCustomerList.Size = new Size(161, 40);
            btnCustomerList.TabIndex = 23;
            btnCustomerList.Text = "Customer List";
            btnCustomerList.UseVisualStyleBackColor = true;
            btnCustomerList.Click += btnCustomerList_Click;
            // 
            // btnMain
            // 
            btnMain.FlatStyle = FlatStyle.Flat;
            btnMain.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnMain.ForeColor = Color.Crimson;
            btnMain.Location = new Point(921, 105);
            btnMain.Name = "btnMain";
            btnMain.Size = new Size(161, 40);
            btnMain.TabIndex = 22;
            btnMain.Text = "Main";
            btnMain.UseVisualStyleBackColor = true;
            btnMain.Click += btnMain_Click;
            // 
            // btnDeleteOrder
            // 
            btnDeleteOrder.FlatStyle = FlatStyle.Flat;
            btnDeleteOrder.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnDeleteOrder.ForeColor = Color.Crimson;
            btnDeleteOrder.Location = new Point(921, 449);
            btnDeleteOrder.Name = "btnDeleteOrder";
            btnDeleteOrder.Size = new Size(161, 40);
            btnDeleteOrder.TabIndex = 20;
            btnDeleteOrder.Text = "Delete";
            btnDeleteOrder.UseVisualStyleBackColor = true;
            // 
            // btnUpdateOrder
            // 
            btnUpdateOrder.FlatStyle = FlatStyle.Flat;
            btnUpdateOrder.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnUpdateOrder.ForeColor = Color.Crimson;
            btnUpdateOrder.Location = new Point(921, 385);
            btnUpdateOrder.Name = "btnUpdateOrder";
            btnUpdateOrder.Size = new Size(161, 40);
            btnUpdateOrder.TabIndex = 19;
            btnUpdateOrder.Text = "Update";
            btnUpdateOrder.UseVisualStyleBackColor = true;
            // 
            // btnCreateOrder
            // 
            btnCreateOrder.FlatStyle = FlatStyle.Flat;
            btnCreateOrder.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnCreateOrder.ForeColor = Color.Crimson;
            btnCreateOrder.Location = new Point(921, 325);
            btnCreateOrder.Name = "btnCreateOrder";
            btnCreateOrder.Size = new Size(161, 40);
            btnCreateOrder.TabIndex = 18;
            btnCreateOrder.Text = "Create";
            btnCreateOrder.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.ForeColor = Color.Crimson;
            label1.Location = new Point(32, 281);
            label1.Name = "label1";
            label1.Size = new Size(157, 41);
            label1.TabIndex = 17;
            label1.Text = "Order List";
            // 
            // dgvOrderList
            // 
            dgvOrderList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvOrderList.Location = new Point(32, 325);
            dgvOrderList.Name = "dgvOrderList";
            dgvOrderList.RowHeadersWidth = 51;
            dgvOrderList.RowTemplate.Height = 29;
            dgvOrderList.Size = new Size(829, 306);
            dgvOrderList.TabIndex = 16;
            // 
            // grbSearch
            // 
            grbSearch.Controls.Add(cboSearch);
            grbSearch.Controls.Add(txtSearch);
            grbSearch.Controls.Add(btnSearch);
            grbSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            grbSearch.ForeColor = Color.Crimson;
            grbSearch.Location = new Point(32, 93);
            grbSearch.Name = "grbSearch";
            grbSearch.Size = new Size(829, 148);
            grbSearch.TabIndex = 15;
            grbSearch.TabStop = false;
            grbSearch.Text = "Search Criteria";
            // 
            // cboSearch
            // 
            cboSearch.FormattingEnabled = true;
            cboSearch.Location = new Point(177, 66);
            cboSearch.Name = "cboSearch";
            cboSearch.Size = new Size(122, 36);
            cboSearch.TabIndex = 10;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(305, 68);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(481, 34);
            txtSearch.TabIndex = 9;
            // 
            // btnSearch
            // 
            btnSearch.FlatStyle = FlatStyle.Flat;
            btnSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnSearch.ForeColor = Color.Crimson;
            btnSearch.Location = new Point(29, 66);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(139, 34);
            btnSearch.TabIndex = 8;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            btnExit.FlatStyle = FlatStyle.Flat;
            btnExit.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnExit.ForeColor = Color.Crimson;
            btnExit.Location = new Point(921, 591);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(161, 40);
            btnExit.TabIndex = 24;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // btnOrderDetail
            // 
            btnOrderDetail.FlatStyle = FlatStyle.Flat;
            btnOrderDetail.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnOrderDetail.ForeColor = Color.Crimson;
            btnOrderDetail.Location = new Point(921, 519);
            btnOrderDetail.Name = "btnOrderDetail";
            btnOrderDetail.Size = new Size(161, 40);
            btnOrderDetail.TabIndex = 25;
            btnOrderDetail.Text = "Detail";
            btnOrderDetail.UseVisualStyleBackColor = true;
            // 
            // frmOrders
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(1129, 655);
            Controls.Add(btnOrderDetail);
            Controls.Add(btnExit);
            Controls.Add(btnCustomerList);
            Controls.Add(btnMain);
            Controls.Add(btnDeleteOrder);
            Controls.Add(btnUpdateOrder);
            Controls.Add(btnCreateOrder);
            Controls.Add(label1);
            Controls.Add(dgvOrderList);
            Controls.Add(grbSearch);
            Controls.Add(lblHeader);
            Name = "frmOrders";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "frmOrders";
            ((System.ComponentModel.ISupportInitialize)dgvOrderList).EndInit();
            grbSearch.ResumeLayout(false);
            grbSearch.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblHeader;
        private Button btnCustomerList;
        private Button btnMain;
        private Button btnDeleteOrder;
        private Button btnUpdateOrder;
        private Button btnCreateOrder;
        private Label label1;
        private DataGridView dgvOrderList;
        private GroupBox grbSearch;
        private ComboBox cboSearch;
        private TextBox txtSearch;
        private Button btnSearch;
        private Button btnExit;
        private Button btnOrderDetail;
    }
}