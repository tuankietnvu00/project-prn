﻿namespace BookStoreApp
{
    partial class frmCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnMain = new Button();
            btnOrderList = new Button();
            btnExit = new Button();
            btnDeleteCustomer = new Button();
            btnUpdateCustomer = new Button();
            btnCreateCustomer = new Button();
            label1 = new Label();
            dgvBookList = new DataGridView();
            grbSearch = new GroupBox();
            cboSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            lblHeader = new Label();
            ((System.ComponentModel.ISupportInitialize)dgvBookList).BeginInit();
            grbSearch.SuspendLayout();
            SuspendLayout();
            // 
            // btnMain
            // 
            btnMain.FlatStyle = FlatStyle.Flat;
            btnMain.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnMain.ForeColor = Color.Crimson;
            btnMain.Location = new Point(928, 212);
            btnMain.Name = "btnMain";
            btnMain.Size = new Size(161, 40);
            btnMain.TabIndex = 23;
            btnMain.Text = "Main";
            btnMain.UseVisualStyleBackColor = true;
            // 
            // btnOrderList
            // 
            btnOrderList.FlatStyle = FlatStyle.Flat;
            btnOrderList.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnOrderList.ForeColor = Color.Crimson;
            btnOrderList.Location = new Point(928, 116);
            btnOrderList.Name = "btnOrderList";
            btnOrderList.Size = new Size(161, 40);
            btnOrderList.TabIndex = 22;
            btnOrderList.Text = "Order List";
            btnOrderList.UseVisualStyleBackColor = true;
            btnOrderList.Click += btnOrderList_Click;
            // 
            // btnExit
            // 
            btnExit.FlatStyle = FlatStyle.Flat;
            btnExit.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnExit.ForeColor = Color.Crimson;
            btnExit.Location = new Point(928, 602);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(161, 40);
            btnExit.TabIndex = 21;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // btnDeleteCustomer
            // 
            btnDeleteCustomer.FlatStyle = FlatStyle.Flat;
            btnDeleteCustomer.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnDeleteCustomer.ForeColor = Color.Crimson;
            btnDeleteCustomer.Location = new Point(928, 495);
            btnDeleteCustomer.Name = "btnDeleteCustomer";
            btnDeleteCustomer.Size = new Size(161, 40);
            btnDeleteCustomer.TabIndex = 20;
            btnDeleteCustomer.Text = "Delete";
            btnDeleteCustomer.UseVisualStyleBackColor = true;
            // 
            // btnUpdateCustomer
            // 
            btnUpdateCustomer.FlatStyle = FlatStyle.Flat;
            btnUpdateCustomer.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnUpdateCustomer.ForeColor = Color.Crimson;
            btnUpdateCustomer.Location = new Point(928, 415);
            btnUpdateCustomer.Name = "btnUpdateCustomer";
            btnUpdateCustomer.Size = new Size(161, 40);
            btnUpdateCustomer.TabIndex = 19;
            btnUpdateCustomer.Text = "Update";
            btnUpdateCustomer.UseVisualStyleBackColor = true;
            // 
            // btnCreateCustomer
            // 
            btnCreateCustomer.FlatStyle = FlatStyle.Flat;
            btnCreateCustomer.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnCreateCustomer.ForeColor = Color.Crimson;
            btnCreateCustomer.Location = new Point(928, 336);
            btnCreateCustomer.Name = "btnCreateCustomer";
            btnCreateCustomer.Size = new Size(161, 40);
            btnCreateCustomer.TabIndex = 18;
            btnCreateCustomer.Text = "Create";
            btnCreateCustomer.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.ForeColor = Color.Crimson;
            label1.Location = new Point(39, 292);
            label1.Name = "label1";
            label1.Size = new Size(210, 41);
            label1.TabIndex = 17;
            label1.Text = "Customer List";
            // 
            // dgvBookList
            // 
            dgvBookList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvBookList.Location = new Point(39, 336);
            dgvBookList.Name = "dgvBookList";
            dgvBookList.RowHeadersWidth = 51;
            dgvBookList.RowTemplate.Height = 29;
            dgvBookList.Size = new Size(829, 306);
            dgvBookList.TabIndex = 16;
            // 
            // grbSearch
            // 
            grbSearch.Controls.Add(cboSearch);
            grbSearch.Controls.Add(txtSearch);
            grbSearch.Controls.Add(btnSearch);
            grbSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            grbSearch.ForeColor = Color.Crimson;
            grbSearch.Location = new Point(39, 104);
            grbSearch.Name = "grbSearch";
            grbSearch.Size = new Size(829, 148);
            grbSearch.TabIndex = 15;
            grbSearch.TabStop = false;
            grbSearch.Text = "Search Criteria";
            // 
            // cboSearch
            // 
            cboSearch.FormattingEnabled = true;
            cboSearch.Location = new Point(177, 66);
            cboSearch.Name = "cboSearch";
            cboSearch.Size = new Size(122, 36);
            cboSearch.TabIndex = 10;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(305, 68);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(481, 34);
            txtSearch.TabIndex = 9;
            // 
            // btnSearch
            // 
            btnSearch.FlatStyle = FlatStyle.Flat;
            btnSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnSearch.ForeColor = Color.Crimson;
            btnSearch.Location = new Point(29, 66);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(139, 34);
            btnSearch.TabIndex = 8;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            // 
            // lblHeader
            // 
            lblHeader.AutoSize = true;
            lblHeader.Font = new Font("Segoe UI", 36F, FontStyle.Bold, GraphicsUnit.Point);
            lblHeader.ForeColor = Color.Crimson;
            lblHeader.Location = new Point(3, 9);
            lblHeader.Name = "lblHeader";
            lblHeader.Size = new Size(347, 81);
            lblHeader.TabIndex = 11;
            lblHeader.Text = "Book Store";
            // 
            // frmCustomers
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(1129, 655);
            Controls.Add(lblHeader);
            Controls.Add(btnMain);
            Controls.Add(btnOrderList);
            Controls.Add(btnExit);
            Controls.Add(btnDeleteCustomer);
            Controls.Add(btnUpdateCustomer);
            Controls.Add(btnCreateCustomer);
            Controls.Add(label1);
            Controls.Add(dgvBookList);
            Controls.Add(grbSearch);
            Name = "frmCustomers";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "frmCustomers";
            ((System.ComponentModel.ISupportInitialize)dgvBookList).EndInit();
            grbSearch.ResumeLayout(false);
            grbSearch.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnMain;
        private Button btnOrderList;
        private Button btnExit;
        private Button btnDeleteCustomer;
        private Button btnUpdateCustomer;
        private Button btnCreateCustomer;
        private Label label1;
        private DataGridView dgvBookList;
        private GroupBox grbSearch;
        private ComboBox cboSearch;
        private TextBox txtSearch;
        private Button btnSearch;
        private Label lblHeader;
    }
}