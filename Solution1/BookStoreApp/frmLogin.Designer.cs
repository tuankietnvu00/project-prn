﻿namespace BookStoreApp
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblLoginHeader = new Label();
            txtEmailLogin = new TextBox();
            lblEmail = new Label();
            txtPasswordLogin = new TextBox();
            lblPassword = new Label();
            btnLogin = new Button();
            SuspendLayout();
            // 
            // lblLoginHeader
            // 
            lblLoginHeader.AutoSize = true;
            lblLoginHeader.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            lblLoginHeader.ForeColor = Color.Crimson;
            lblLoginHeader.Location = new Point(189, 9);
            lblLoginHeader.Name = "lblLoginHeader";
            lblLoginHeader.Size = new Size(164, 41);
            lblLoginHeader.TabIndex = 0;
            lblLoginHeader.Text = "BookStore";
            // 
            // txtEmailLogin
            // 
            txtEmailLogin.Font = new Font("Segoe UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            txtEmailLogin.Location = new Point(170, 96);
            txtEmailLogin.Name = "txtEmailLogin";
            txtEmailLogin.Size = new Size(301, 38);
            txtEmailLogin.TabIndex = 1;
            // 
            // lblEmail
            // 
            lblEmail.AutoSize = true;
            lblEmail.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            lblEmail.ForeColor = Color.Crimson;
            lblEmail.Location = new Point(12, 96);
            lblEmail.Name = "lblEmail";
            lblEmail.Size = new Size(151, 41);
            lblEmail.TabIndex = 2;
            lblEmail.Text = "Email      :";
            // 
            // txtPasswordLogin
            // 
            txtPasswordLogin.Font = new Font("Segoe UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            txtPasswordLogin.Location = new Point(170, 180);
            txtPasswordLogin.Name = "txtPasswordLogin";
            txtPasswordLogin.PasswordChar = '*';
            txtPasswordLogin.Size = new Size(301, 38);
            txtPasswordLogin.TabIndex = 4;
            // 
            // lblPassword
            // 
            lblPassword.AutoSize = true;
            lblPassword.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            lblPassword.ForeColor = Color.Crimson;
            lblPassword.Location = new Point(6, 175);
            lblPassword.Name = "lblPassword";
            lblPassword.Size = new Size(158, 41);
            lblPassword.TabIndex = 7;
            lblPassword.Text = "Password:";
            // 
            // btnLogin
            // 
            btnLogin.FlatStyle = FlatStyle.Flat;
            btnLogin.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnLogin.ForeColor = Color.Crimson;
            btnLogin.Location = new Point(229, 257);
            btnLogin.Name = "btnLogin";
            btnLogin.Size = new Size(124, 40);
            btnLogin.TabIndex = 10;
            btnLogin.Text = "Login";
            btnLogin.UseVisualStyleBackColor = true;
            btnLogin.Click += btnLogin_Click;
            // 
            // frmLogin
            // 
            AcceptButton = btnLogin;
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(561, 333);
            Controls.Add(btnLogin);
            Controls.Add(lblPassword);
            Controls.Add(txtPasswordLogin);
            Controls.Add(lblEmail);
            Controls.Add(txtEmailLogin);
            Controls.Add(lblLoginHeader);
            Name = "frmLogin";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Login";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblLoginHeader;
        private TextBox txtEmailLogin;
        private Label lblEmail;
        private TextBox txtPasswordLogin;
        private Label lblPassword;
        private Button btnLogin;
    }
}